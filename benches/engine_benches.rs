use criterion::{criterion_group, criterion_main, BatchSize, Criterion, ParameterizedBenchmark};
use kvs::{KvStore, KvsEngine, SledKvsEngine};
use rand::prelude::*;
use tempfile::TempDir;
use rand::{Rng, distributions::Alphanumeric};
use rand::rngs::SmallRng;
use std::iter;

fn rand_string(rng: &mut SmallRng) -> String {
    let key_length: usize = rng.gen_range(1, 100000);
    rng
        .sample_iter(&Alphanumeric)
        .take(key_length)
        .collect()
}

fn set_bench(c: &mut Criterion) {
    let bench = ParameterizedBenchmark::new(
        "kvs",
        |b, _| {
            b.iter_batched(
                || {
                    let mut rng = SmallRng::from_entropy();
                    let mut pairs: Vec<(String, String)> = vec![];
                    pairs.reserve(100);
                    for _ in 0..100 {
                        let key = rand_string(&mut rng);
                        let value = rand_string(&mut rng);
                        pairs.push((key, value));
                    }

                    let temp_dir = TempDir::new().expect("tempdir");
                    let store = KvStore::open(temp_dir.path()).expect("open KvStore");
                    
                    (pairs, temp_dir, store)
                },
                |(pairs, _, mut store)| {
                    for (key, value) in pairs {
                        store.set(key, value).expect("store.set");
                    }
                },
                BatchSize::SmallInput,
            );
        },
        iter::once(()),
    )
    .with_function("sled", |b, _| {
        b.iter_batched(
            || {
                let mut rng = SmallRng::from_entropy();
                let mut pairs: Vec<(String, String)> = vec![];
                pairs.reserve(100);
                for _ in 0..100 {
                    let key = rand_string(&mut rng);
                    let value = rand_string(&mut rng);
                    pairs.push((key, value));
                }
                
                let temp_dir = TempDir::new().expect("tempdir");
                let store = SledKvsEngine::open(temp_dir.path()).expect("open SledKvsEngine");
                
                (pairs, temp_dir, store)
            },
            |(pairs, _, mut store)| {
                for (key, value) in pairs {
                    store.set(key, value).expect("store.set");
                }
            },
            BatchSize::SmallInput,
        );
    });
    c.bench("set_bench", bench);
}

fn get_bench(c: &mut Criterion) {
    let bench = ParameterizedBenchmark::new(
        "kvs",
        |b, _| {
            b.iter_batched(|| {
                let temp_dir = TempDir::new().expect("tempdir");
                let mut store = KvStore::open(temp_dir.path()).expect("open KvStore");
                
                let mut rng = SmallRng::from_entropy();
                let mut keys: Vec<String> = vec![];
                keys.reserve(1000);
                for _ in 0..1000 {
                    let key = rand_string(&mut rng);
                    let value = rand_string(&mut rng);
                    store.set(key.clone(), value).expect("store.set");
                    keys.push(key);
                }

                (keys, temp_dir, store)
            },
            |(keys, _, mut store)| {
                for key in keys.into_iter() {
                    store.get(key).expect("store.get");
                }
            }, BatchSize::SmallInput);
        },
        iter::once(()),
    )
    .with_function("sled", |b, _| {
        b.iter_batched(|| {
            let temp_dir = TempDir::new().expect("tempdir");
            let mut store = SledKvsEngine::open(temp_dir.path()).expect("open SledKvsEngine");
            
            let mut rng = SmallRng::from_entropy();
            let mut keys: Vec<String> = vec![];
            keys.reserve(1000);
            for _ in 0..1000 {
                let key = rand_string(&mut rng);
                let value = rand_string(&mut rng);
                store.set(key.clone(), value).expect("store.set");
                keys.push(key);
            }

            (keys, temp_dir, store)
        },
        |(keys, _, mut store)| {
            for key in keys.into_iter() {
                store.get(key).expect("store.get");
            }
        }, BatchSize::SmallInput);
    });
    c.bench("get_bench", bench);
}

criterion_group!(benches, set_bench, get_bench);
criterion_main!(benches);
