use super::{KvStore, KvsEngine, KvsError, Result, SledKvsEngine};
use crate::protocol::{Command, CommandType, ErrorType, Response};
use log::debug;
use std::io::Write;
use std::net::{TcpListener, TcpStream};

/// Engine types.
#[derive(Debug)]
pub enum Engine {
    /// Standard key-value store engine.
    Kvs,
    /// Sled engine.
    Sled,
}

/// The KVS server.
pub struct KvsServer {
    store: Box<KvsEngine>,
    address: String,
}

impl KvsServer {
    /// Constructor.
    pub fn new(engine: Engine, address: &str) -> Result<Self> {
        let cur_dir = std::env::current_dir()?;
        let store: Box<KvsEngine> = match engine {
            Engine::Kvs => Box::new(KvStore::open(&cur_dir)?),
            Engine::Sled => Box::new(SledKvsEngine::open(&cur_dir)?),
        };
        Ok(Self {
            store,
            address: address.to_string(),
        })
    }

    /// Start listening for connections.
    pub fn listen(&mut self) -> Result<()> {
        debug!("Starting TCP listener");
        let listener = TcpListener::bind(&self.address)?;
        for stream in listener.incoming() {
            self.handle_incoming(stream?)?;
        }
        Ok(())
    }

    fn handle_incoming(&mut self, mut stream: TcpStream) -> Result<()> {
        debug!("Handling incoming connection");
        let cmd: Command = bincode::deserialize_from(&stream)?;
        match cmd.r#type {
            CommandType::Get => {
                debug!("Client requests to get '{}'", cmd.key);
                let resp = match self.store.get(cmd.key) {
                    Ok(value) => Response {
                        value,
                        error: None,
                        error_message: None,
                    },
                    Err(err) => {
                        let err_type = match err {
                            KvsError::NonExistentKey(_) => ErrorType::NonExistentKey,
                            _ => ErrorType::Unknown,
                        };
                        Response {
                            value: None,
                            error: Some(err_type),
                            error_message: Some(err.to_string()),
                        }
                    }
                };
                let serialized_resp = bincode::serialize(&resp)?;
                stream.write(&serialized_resp)?;
            }
            CommandType::Set => {
                debug!("Client requests to set '{}' => '{}'", cmd.key, cmd.value);
                let resp = match self.store.set(cmd.key, cmd.value) {
                    Ok(()) => Response {
                        value: None,
                        error: None,
                        error_message: None,
                    },
                    Err(err) => Response {
                        value: None,
                        error: Some(ErrorType::Unknown),
                        error_message: Some(err.to_string()),
                    },
                };
                let serialized_resp = bincode::serialize(&resp)?;
                stream.write(&serialized_resp)?;
            }
            CommandType::Remove => {
                debug!("Client requests to remove '{}'", cmd.key);
                let resp = match self.store.remove(cmd.key) {
                    Ok(()) => Response {
                        value: None,
                        error: None,
                        error_message: None,
                    },
                    Err(err) => {
                        let err_type = match err {
                            KvsError::NonExistentKey(_) => ErrorType::NonExistentKey,
                            _ => ErrorType::Unknown,
                        };
                        Response {
                            value: None,
                            error: Some(err_type),
                            error_message: Some(err.to_string()),
                        }
                    }
                };
                let serialized_resp = bincode::serialize(&resp)?;
                stream.write(&serialized_resp)?;
            }
        };
        Ok(())
    }
}
