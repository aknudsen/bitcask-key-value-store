use serde::{Deserialize, Serialize};

/// Type of server command.
#[derive(Serialize, Deserialize, Debug)]
pub enum CommandType {
    Get,
    Set,
    Remove,
}

/// Server command.
#[derive(Serialize, Deserialize, Debug)]
pub struct Command {
    pub r#type: CommandType,
    pub key: String,
    pub value: String,
}

/// Error type of server response.
#[derive(Serialize, Deserialize, Debug)]
pub enum ErrorType {
    NonExistentKey,
    Unknown,
}

/// Server response.
#[derive(Serialize, Deserialize, Debug)]
pub struct Response {
    pub value: Option<String>,
    pub error: Option<ErrorType>,
    pub error_message: Option<String>,
}
