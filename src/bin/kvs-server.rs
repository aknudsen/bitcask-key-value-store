use clap::Arg;
use kvs::{arg_parser, setup_logging, Engine, KvsError, KvsServer, Result};
use log::{debug, info};

fn main() -> Result<()> {
    let app = arg_parser!("KVS Server")
        .arg(
            Arg::with_name("addr")
                .long("addr")
                .value_name("IP-PORT")
                .takes_value(true)
                .default_value("127.0.0.1:4000")
                .help("Specify listening address"),
        )
        .arg(
            Arg::with_name("engine")
                .long("engine")
                .value_name("ENGINE")
                .takes_value(true)
                .possible_values(&["kvs", "sled"])
                .help("Specify engine [default: kvs]"),
        );
    let matches = app.get_matches();

    setup_logging(matches.occurrences_of("verbose"))?;

    let engine = if matches.is_present("engine") {
        debug!("Determining storage engine from user argument");
        match matches.value_of("engine").expect("engine") {
            "kvs" => Engine::Kvs,
            "sled" => Engine::Sled,
            _ => panic!("unrecognized engine"),
        }
    } else {
        let fpath = std::env::current_dir()?.join(".kvs/.engine");
        debug!(
            "Determining storage engine from previously used one, encoded in {:?}",
            fpath
        );
        if fpath.exists() {
            let prev_engine_str = std::fs::read_to_string(fpath)?;
            let prev_engine = match prev_engine_str.to_lowercase().as_ref() {
                "kvs" => Engine::Kvs,
                "sled" => Engine::Sled,
                _ => {
                    return Err(KvsError::ServerError(format!(
                        "Unrecognized previous storage engine: '{}'",
                        prev_engine_str
                    )));
                }
            };
            debug!(
                "Determined previously used storage engine: {:?}",
                prev_engine
            );
            prev_engine
        } else {
            Engine::Kvs
        }
    };
    let addr = matches.value_of("addr").expect("addr");
    let version = env!("CARGO_PKG_VERSION");
    info!(
        "KVS Server version {} starting, engine: {:?}, address: {}",
        version, engine, addr
    );

    let mut server = KvsServer::new(engine, addr)?;
    server.listen()?;

    Ok(())
}
