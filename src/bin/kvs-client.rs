use clap::{AppSettings, Arg, SubCommand};
use kvs::{arg_parser, setup_logging, KvsClient, KvsError, Result};

fn main() -> Result<()> {
    let addr_arg = Arg::with_name("addr")
        .long("addr")
        .value_name("IP-PORT")
        .takes_value(true)
        .default_value("127.0.0.1:4000")
        .help("Specify server address");
    let app = arg_parser!("KVS Client").setting(AppSettings::SubcommandRequiredElseHelp);
    let matches = app
        .subcommand(
            SubCommand::with_name("get")
                .about("gets a key")
                .arg(&addr_arg)
                .arg(Arg::with_name("key").required(true).help("Key to get")),
        )
        .subcommand(
            SubCommand::with_name("set")
                .about("sets a key")
                .arg(&addr_arg)
                .arg(Arg::with_name("key").required(true).help("Key to set"))
                .arg(Arg::with_name("value").required(true).help("Value to set")),
        )
        .subcommand(
            SubCommand::with_name("rm")
                .about("removes a key")
                .arg(&addr_arg)
                .arg(Arg::with_name("key").required(true).help("Key to remove")),
        )
        .get_matches();

    setup_logging(matches.occurrences_of("verbose"))?;

    match matches.subcommand_name() {
        Some("get") => {
            let sub_matches = matches.subcommand_matches("get").expect("get");
            let key = sub_matches.value_of("key").expect("key");
            let addr = sub_matches.value_of("addr").expect("addr");
            let mut client = KvsClient::new(addr)?;

            match client.get(key)? {
                Some(value) => {
                    println!("{}", value);
                }
                None => {
                    println!("Key not found");
                }
            };

            Ok(())
        }
        Some("set") => {
            let sub_matches = matches.subcommand_matches("set").expect("set");
            let key = sub_matches.value_of("key").expect("key");
            let value = sub_matches.value_of("value").expect("value");
            let addr = sub_matches.value_of("addr").expect("addr");
            let mut client = KvsClient::new(addr)?;

            client.set(key, value)
        }
        Some("rm") => {
            let sub_matches = matches.subcommand_matches("rm").unwrap();
            let key = sub_matches.value_of("key").unwrap();
            let addr = sub_matches.value_of("addr").expect("addr");
            let mut client = KvsClient::new(addr)?;

            client.remove(key).map_err(|err| match err {
                KvsError::NonExistentKey(msg) => {
                    eprintln!("{}", msg);
                    std::process::exit(1);
                }
                _ => err,
            })
        }
        _ => panic!("Unrecognized subcommand"),
    }
}
