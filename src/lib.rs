#![deny(missing_docs)]
#![crate_name = "kvs"]

//! Key-Value Store
use log::LevelFilter;

mod client;
mod engines;
mod protocol;
mod server;

pub use client::KvsClient;
pub use engines::{KvStore, KvsEngine, SledKvsEngine};
pub use server::{Engine, KvsServer};

use failure::Fail;
use std::io;

/// Macro for creating argument parser.
#[macro_export]
macro_rules! arg_parser {
    ($name:expr) => {{
        use clap::{App, Arg};
        App::new($name)
            .version(env!("CARGO_PKG_VERSION"))
            .author(env!("CARGO_PKG_AUTHORS"))
            .about(env!("CARGO_PKG_DESCRIPTION"))
            .arg(
                Arg::with_name("verbose")
                    .short("v")
                    .multiple(true)
                    .help("Increase verbosity"),
            )
    }};
}

/// Our error type.
#[derive(Fail, Debug)]
pub enum KvsError {
    /// Non-existent key.
    #[fail(display = "Non-existent key: {}", _0)]
    NonExistentKey(String),
    /// Server error.
    #[fail(display = "Server error: {}", _0)]
    ServerError(String),
    /// Another storage engine was used before.
    #[fail(display = "{}", _0)]
    WrongEngine(String),
    /// An unknown error occurred.
    #[fail(display = "{}", _0)]
    UnknownError(String),

    /// An IO error occurred.
    #[fail(display = "{}", _0)]
    IoError(#[fail(cause)] io::Error),
    /// A Bincode error occurred.
    #[fail(display = "Bincode error: {}", _0)]
    BincodeError(#[fail(cause)] bincode::Error),
    /// A SetLoggerError occurred.
    #[fail(display = "{}", _0)]
    SetLoggerError(#[fail(cause)] log::SetLoggerError),
    /// A Sled error occurred.
    #[fail(display = "Sled error: {}", _0)]
    SledError(String),
}

impl From<io::Error> for KvsError {
    fn from(err: io::Error) -> KvsError {
        KvsError::IoError(err)
    }
}

impl From<bincode::Error> for KvsError {
    fn from(err: bincode::Error) -> KvsError {
        KvsError::BincodeError(err)
    }
}

impl From<log::SetLoggerError> for KvsError {
    fn from(err: log::SetLoggerError) -> KvsError {
        KvsError::SetLoggerError(err)
    }
}

/// Result type.
pub type Result<T> = std::result::Result<T, KvsError>;

/// Set up logging subsystem.
pub fn setup_logging(verbosity: u64) -> Result<()> {
    let level = match verbosity {
        0 => {
            let level_str = std::env::var("RUST_LOG")
                .unwrap_or("info".to_string())
                .to_lowercase();
            match level_str.as_str() {
                "info" => LevelFilter::Info,
                "debug" => LevelFilter::Debug,
                "trace" => LevelFilter::Trace,
                "error" => LevelFilter::Error,
                "warn" => LevelFilter::Warn,
                "off" => LevelFilter::Off,
                _ => {
                    return Err(KvsError::UnknownError(format!(
                        "unrecognized log level: '{}'",
                        level_str
                    )));
                }
            }
        }
        1 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    fern::Dispatch::new()
        .level(level)
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .chain(std::io::stderr())
        .apply()?;
    Ok(())
}
