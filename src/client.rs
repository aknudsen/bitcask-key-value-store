use super::Result;
use crate::protocol::{Command, CommandType, ErrorType, Response};
use crate::KvsError;
use log::debug;
use std::io::Write;
use std::net::TcpStream;

/// The KVS client.
pub struct KvsClient {
    stream: TcpStream,
}

impl KvsClient {
    /// Constructor.
    pub fn new(addr: &str) -> Result<Self> {
        let stream = TcpStream::connect(addr)?;
        debug!("Successfully connected to server");
        Ok(Self { stream })
    }

    /// Get the value for a key.
    pub fn get(&mut self, key: &str) -> Result<Option<String>> {
        debug!("Getting key '{}' from server", key);
        let cmd = Command {
            r#type: CommandType::Get,
            key: key.to_string(),
            value: String::new(),
        };
        let serialized = bincode::serialize(&cmd)?;
        self.stream.write(&serialized)?;
        let resp: Response = bincode::deserialize_from(&self.stream)?;
        match resp.error {
            None => Ok(resp.value),
            Some(_) => {
                let err_msg = resp.error_message.expect("resp.error_message");
                debug!("Server returned an error: {}", err_msg);
                Err(KvsError::ServerError(err_msg))
            }
        }
    }

    /// Set the value for a key.
    pub fn set(&mut self, key: &str, value: &str) -> Result<()> {
        debug!("Setting key '{}' => '{}' in server", key, value);
        let cmd = Command {
            r#type: CommandType::Set,
            key: key.to_string(),
            value: value.to_string(),
        };
        let serialized = bincode::serialize(&cmd)?;
        self.stream.write(&serialized)?;
        let resp: Response = bincode::deserialize_from(&self.stream)?;
        match resp.error {
            None => Ok(()),
            Some(_) => {
                let err_msg = resp.error_message.expect("resp.error_message");
                debug!("Server returned an error: {}", err_msg);
                Err(KvsError::ServerError(err_msg))
            }
        }
    }

    /// Remove a key value pair.
    pub fn remove(&mut self, key: &str) -> Result<()> {
        debug!("Removing key '{}' from server", key);
        let cmd = Command {
            r#type: CommandType::Remove,
            key: key.to_string(),
            value: String::new(),
        };
        let serialized = bincode::serialize(&cmd)?;
        self.stream.write(&serialized)?;
        let resp: Response = bincode::deserialize_from(&self.stream)?;
        match resp.error {
            None => Ok(()),
            Some(err) => {
                let err_msg = resp.error_message.expect("resp.error_message");
                debug!("Server returned error: {}", err_msg);
                match err {
                    ErrorType::NonExistentKey => {
                        eprintln!("Key not found");
                        std::process::exit(1);
                    }
                    _ => Err(KvsError::ServerError(err_msg)),
                }
            }
        }
    }
}
