use super::Result;
use crate::KvsError;
use log::debug;
use sled::Db;
use std::path::Path;

use super::open_engine_dir;

/// Sled implementation of KvsEngine.
pub struct SledKvsEngine {
    tree: Db,
}

impl SledKvsEngine {
    /// Constructor.
    pub fn open(dpath: &Path) -> Result<Self> {
        let dpath = open_engine_dir(dpath, "sled")?;
        debug!("Opening SledKvsEngine, dpath: '{}'", dpath);
        let tree = Db::start_default(dpath).map_err(|err| KvsError::SledError(err.to_string()))?;
        Ok(Self { tree })
    }
}

impl super::KvsEngine for SledKvsEngine {
    fn set(&mut self, key: String, value: String) -> Result<()> {
        debug!("Setting '{}' => '{}'", key, value);
        self.tree
            .set(key.as_bytes(), value.as_bytes())
            .map_err(|err| KvsError::SledError(err.to_string()))?;
        self.tree
            .flush()
            .map_err(|err| KvsError::SledError(err.to_string()))?;
        Ok(())
    }

    fn get(&mut self, key: String) -> Result<Option<String>> {
        debug!("Getting key '{}'", key);
        let data = self
            .tree
            .get(key.as_bytes())
            .map_err(|err| KvsError::SledError(err.to_string()))?;
        Ok(data.map(|v| String::from_utf8(v.as_ref().to_vec()).expect("from_utf8")))
    }

    fn remove(&mut self, key: String) -> Result<()> {
        debug!("Removing value for key '{}'", key);
        let prev = self
            .tree
            .del(key.as_bytes())
            .map_err(|err| KvsError::SledError(err.to_string()))?;
        self.tree
            .flush()
            .map_err(|err| KvsError::SledError(err.to_string()))?;
        match prev {
            Some(_) => {
                debug!("Successfully removed key/value pair");
                Ok(())
            }
            _ => {
                debug!("This key didn't exist");
                Err(KvsError::NonExistentKey(key))
            }
        }
    }
}
