use log::debug;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::{self, create_dir_all, File, OpenOptions};
use std::io::{Seek, SeekFrom, Write};
use std::path::Path;
use std::sync::Arc;

use super::super::{KvsError, Result};
use super::open_engine_dir;

const COMPACTION_THRESHOLD: u64 = 1024 * 1024;

/// Key-value store
#[derive(Debug)]
pub struct KvStore {
    dpath: String,
    index: Option<HashMap<String, LogPointer>>,
    file: Option<Arc<File>>,
    uncompacted: u64,
}

#[derive(Serialize, Deserialize, Debug)]
enum CommandType {
    Set,
    Remove,
}

#[derive(Serialize, Deserialize, Debug)]
struct Command {
    typ: CommandType,
    key: String,
    value: String,
}

#[derive(Debug)]
struct LogPointer {
    file: Arc<File>,
    offset: u64,
    length: u64,
}

fn read_value(key: &String, index: &HashMap<String, LogPointer>) -> Result<Option<String>> {
    match index.get(key).as_mut() {
        None => Ok(None),
        Some(lp) => {
            let mut file = lp.file.as_ref();
            debug!("Reading from file offset {}: {:?}", lp.offset, file);
            file.seek(SeekFrom::Start(lp.offset))?;
            let cmd: Command = bincode::deserialize_from(file)?;
            Ok(Some(cmd.value))
        }
    }
}

impl KvStore {
    /// Open a KvStore for a certain directory.
    pub fn open(dpath: &Path) -> Result<Self> {
        let dpath_str = open_engine_dir(dpath, "kvs")?;
        debug!("Opening KvStore, dpath: '{}'", dpath_str);
        Ok(Self {
            index: None,
            file: None,
            dpath: dpath_str,
            uncompacted: 0,
        })
    }

    fn build_index(self: &mut KvStore) -> Result<()> {
        if self.index.is_some() {
            debug!("Index already defined");
            return Ok(());
        }

        self.index = Some(HashMap::new());
        let index = self.index.as_mut().expect("index should be defined");

        // Read all available logs
        let mut uncompacted: u64 = 0;
        let mut i = 1;
        loop {
            let fpath = Path::new(&self.dpath).join(format!("log-{}", i));
            let file_rslt = File::open(fpath.clone());
            if file_rslt.is_err() {
                debug!(
                    "File '{}' doesn't exist, ending loop",
                    fpath.to_str().unwrap()
                );
                break;
            }

            let file = Arc::new(file_rslt.expect("file should be opened"));
            debug!(
                "File '{}' exists, applying to index",
                fpath.to_str().unwrap()
            );
            loop {
                let offset = file.as_ref().seek(SeekFrom::Current(0))?;
                let read_rslt = bincode::deserialize_from(file.as_ref());
                if read_rslt.is_err() {
                    break;
                }

                let cmd: Command = read_rslt.expect("read result should be OK");
                let cur_offset = file.as_ref().seek(SeekFrom::Current(0))?;
                let cmd_length: u64 = cur_offset - offset;
                match cmd.typ {
                    CommandType::Set => {
                        debug!("Read set command {} => {}", cmd.key, cmd.value);
                        if let Some(old_ptr) = index.insert(
                            cmd.key,
                            LogPointer {
                                file: file.clone(),
                                offset,
                                length: cmd_length,
                            },
                        ) {
                            // An overridden command can be compacted
                            debug!("Overridden command can be compacted: {}", old_ptr.length);
                            uncompacted += old_ptr.length;
                        }
                    }
                    CommandType::Remove => {
                        debug!("Read remove command {}", cmd.key);
                        if let Some(old_ptr) = index.remove(&cmd.key) {
                            debug!("Remove command invalidated old command, which can be compacted: {}",
                                old_ptr.length);
                            uncompacted += old_ptr.length;
                        }

                        // Remove commands can be compacted
                        debug!("Remove command can be compacted: {}", cmd_length);
                        uncompacted += cmd_length;
                    }
                };
            }

            i += 1;
        }

        debug!("Calculated compaction potential: {}", uncompacted);
        self.uncompacted = uncompacted;

        debug!("Successfully built index: {:?}", index);
        Ok(())
    }

    fn write_command(self: &mut KvStore, cmd: Command) -> Result<()> {
        if self.file.is_none() {
            let mut i = 1;
            loop {
                let fpath = Path::new(&self.dpath).join(format!("log-{}", i));
                if !fpath.exists() {
                    debug!("Creating file at {}", fpath.to_str().unwrap());
                    let file = OpenOptions::new()
                        .read(true)
                        .write(true)
                        .create_new(true)
                        .open(fpath)?;
                    self.file = Some(Arc::new(file));
                    break;
                }

                i += 1;
            }
        }

        let serialized = bincode::serialize(&cmd)?;
        let file = self.file.as_ref().expect("self.file");
        let offset = file.as_ref().seek(SeekFrom::End(0))?;
        file.as_ref().write(&serialized)?;
        let cur_offset = file.as_ref().seek(SeekFrom::End(0))?;

        match cmd.typ {
            CommandType::Remove => {}
            CommandType::Set => {
                debug!("Writing command to file {:?}", self.file.as_ref());
                let file = self.file.as_ref().unwrap().clone();
                let lp = LogPointer {
                    file,
                    offset,
                    length: cur_offset - offset,
                };
                self.index
                    .as_mut()
                    .expect("self.index should be defined")
                    .insert(cmd.key, lp);
            }
        };

        if self.uncompacted > COMPACTION_THRESHOLD {
            self.compact()?;
        } else {
            debug!(
                "Uncompacted data ({}) <= threshold ({})",
                self.uncompacted, COMPACTION_THRESHOLD
            );
        }
        Ok(())
    }

    fn compact(&mut self) -> Result<()> {
        debug!("Compacting log files");
        let index = self.index.as_ref().expect("self.index should be defined");

        let new_dpath = format!("{}.new", self.dpath);
        create_dir_all(&new_dpath)?;
        fs::write(Path::new(&new_dpath).join(".engine"), "kvs")?;
        let mut file = File::create(Path::new(&new_dpath).join("log-1"))?;
        for (key, _) in self.index.as_ref().expect("self.index should be defined") {
            let value = read_value(key, index)?.expect("key should have value");
            let cmd = Command {
                typ: CommandType::Set,
                key: key.to_owned(),
                value,
            };
            let serialized = bincode::serialize(&cmd)?;
            debug!("Wrote command to compacted log file: {:?}", cmd);
            file.write(&serialized)?;
        }

        file.flush()?;
        debug!("Finished writing new log file");

        self.index = None;
        self.file = None;
        let old_dpath = format!("{}.old", self.dpath);
        debug!("Moving {} to {}", self.dpath, old_dpath);
        fs::rename(&self.dpath, &old_dpath)?;
        debug!("Moving {} to {}", new_dpath, self.dpath);
        fs::rename(&new_dpath, &self.dpath)?;
        debug!("Removing {}", old_dpath);
        fs::remove_dir_all(&old_dpath)?;

        self.build_index()?;

        self.uncompacted = 0;
        Ok(())
    }
}

impl super::KvsEngine for KvStore {
    fn set(self: &mut KvStore, key: String, val: String) -> Result<()> {
        debug!("Setting '{}' => '{}'", key, val);
        self.build_index()?;
        let index = self.index.as_ref().expect("index undefined");
        let existing_val = read_value(&key, index)?;
        if existing_val.as_ref() == Some(&val) {
            debug!("Doing nothing since the existing value is the same");
            return Ok(());
        }

        if existing_val.is_some() {
            let old_lp = index.get(&key).expect("key should be in index");
            debug!("Adding to compaction potential: {}", old_lp.length);
            self.uncompacted += old_lp.length;
        }

        let cmd = Command {
            typ: CommandType::Set,
            key: key.clone(),
            value: val.clone(),
        };
        debug!("Writing set command: {}, {}", key, val);
        self.write_command(cmd)?;
        Ok(())
    }

    fn get(self: &mut KvStore, key: String) -> Result<Option<String>> {
        debug!("Getting key '{}'", key);
        self.build_index()?;
        let index = self.index.as_ref().unwrap();
        debug!("Getting key '{}' from index {:?}", key, index);
        let val = read_value(&key, index)?;
        debug!("Returning value {:?}", val);
        Ok(val)
    }

    fn remove(self: &mut KvStore, key: String) -> Result<()> {
        debug!("Removing value for key '{}'", key);

        self.build_index()?;
        let index = self.index.as_mut().unwrap();
        if !index.contains_key(&key) {
            return Err(KvsError::NonExistentKey(key))?;
        }

        let old_cmd = index.remove(&key).expect("key not found");
        self.uncompacted += old_cmd.length;
        let cmd = Command {
            typ: CommandType::Remove,
            key: key.clone(),
            value: String::new(),
        };
        debug!("Writing remove command: {}", key);
        self.write_command(cmd)?;
        Ok(())
    }
}
