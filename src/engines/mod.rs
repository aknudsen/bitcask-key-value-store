use crate::KvsError;
use std::fs::{self, create_dir_all, read_to_string};
use std::path::Path;

mod kvstore;
mod sledkvsengine;

use super::Result;
pub use kvstore::KvStore;
pub use sledkvsengine::SledKvsEngine;

/// Storage engine interface.
pub trait KvsEngine {
    /// Set a key.
    fn set(&mut self, key: String, value: String) -> Result<()>;
    /// Get a key.
    fn get(&mut self, key: String) -> Result<Option<String>>;
    /// Remove a key.
    fn remove(&mut self, key: String) -> Result<()>;
}

/// Open a directory containing engine state.
fn open_engine_dir(dpath: &Path, engine: &str) -> Result<String> {
    let dpath_full = dpath.join(".kvs");
    let engine_fpath = dpath_full.join(".engine");
    if !dpath_full.exists() {
        create_dir_all(&dpath_full)?;
        fs::write(engine_fpath, engine)?;
    } else {
        let prev_engine = read_to_string(engine_fpath)?.to_lowercase();
        if prev_engine != engine.to_lowercase() {
            return Err(KvsError::WrongEngine(format!(
                "This directory was initialized with another storage engine: {}",
                prev_engine
            )));
        }
    }
    Ok(dpath_full
        .canonicalize()?
        .to_str()
        .expect("directory should exist")
        .to_string())
}
