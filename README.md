# Bitcask Key-Value Store Implemented in Rust
Key-value store based on the Bitcask algorithm, written in Rust. Solution to Pingcap's
[Practical Networked Applications in Rust](https://github.com/pingcap/talent-plan/blob/master/rust/docs/lesson-plan.md#practical-networked-applications-in-rust)
course.
